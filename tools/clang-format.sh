#!/bin/bash

clang-format --dry-run -Werror $(git ls-files | grep -E ".*\.(cpp|h)")
