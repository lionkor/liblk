#!/bin/bash

cppcheck --platform=unix64 --error-exitcode=1 --language=c++ --std=c++20 -I./include --enable=warning,performance,portability $(git ls-files | grep -E ".*\.(cpp|h)")
