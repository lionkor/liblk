# liblk

[![pipeline status](https://gitlab.com/lionkor/liblk/badges/master/pipeline.svg)](https://gitlab.com/lionkor/liblk/-/commits/master) [![](https://img.shields.io/badge/Support%20my%20Work-Patreon-%23ff424d)](https://patreon.com/lion_kor) [![](https://img.shields.io/badge/Support%20my%20Work-PayPal-%230079c1%20)](https://www.paypal.com/donate?hosted_button_id=BHWMH7GDX35QS)

A library with multiple helper classes and functions, useful when developing common desktop- and backend-applications. Fully C++20.

## What's included?

- [`lk::Logger`](./include/lk/Logger.h) - A logging class which supports asynchronously logging to multiple streams - in small-scale benchmarks far outperforms `std::cout`, as it buffers output, and then writes them to the stream(s) in another thread on flush. [→ Example](./examples/log-example.cpp)
- [`lk::ArgsParser`](./include/lk/ArgsParser.h) - A parser for commandline arguments of the forms `--arg`, `--arg=<value>`. [→ Example](./examples/args-example.cpp)
- [`lk::serialize`, `lk::deserialize`](./include/lk/Serialization.h) - A stupidly fast flat binary de/serializer (endianness-aware), useful for high performance networking and IPC. Includes an any-size-integer endianness conversion function (`lk::byteswap`), floating-point serialization, and a few other helpful features.

## How to use

Requires CMake >=3.16 and a C++20-compatible compiler.

### Library

Add the library as a CMake subdirectory. To do this, follow the steps:

1. Clone this library somewhere. That place will now be referred to as `THAT_PATH`. For example, this readme should be at `THAT_PATH/README.md`.
2. Add to your CMakeLists.txt: `add_subdirectory("THAT_PATH")`, and to your target (let's call this `my-target`), `target_link_libraries(my-target lk)`.

*Example* of a resulting CMakeLists.txt, assuming the library was cloned into the same folder as the CMakeLists.txt:

```cmake
project(my-project)
add_subdirectory("./liblk") # <- !
add_executable(my-target src/... include/...) # deliberately not specified for demonstration's sake
target_link_libraries(my-target lk) # <- !
```

Now, you can `#include <lk/...>` any of the headers in the library.

### Examples

1. Run CMake with `BUILD_EXAMPLES` turned on, for example: `cmake . -DBUILD_EXAMPLES=ON`
2. Build with `cmake --build . --parallel`
3. You can now run `./args-example`, `./log-example`, etc.

