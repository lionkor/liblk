#include <iostream>
#include <lk/Logger.h>
#include <string>

int main() {
    // first, add streams that should be written to
    lk::Logger::the().add_stream(std::cout);
    // you can use filenames here, or open a stream and add it via the
    // method above.
    lk::Logger::the().add_file_stream("log-example.log");

    // lk::Logger will write ONLY on a flush(), so end each output with std::endl,
    // std::flush, or similar (as desired). This has to be in such a way that flush() is
    // called at least once before the program ends. In the following example we will
    // flush after every line.

    lk::Logger::stream() << "This is written to the stream raw, like to std::cout, but also to the file." << std::endl;
    lk::Logger::stream() << "This can write any std::ostream operator<<() implementing object: " << std::hex << 5.2 << std::endl;

    // of course you can write to the stream like any other with .write()
    std::string test = "Hello!\n";
    lk::Logger::stream().write(test.data(), test.size());
    // don't forget to flush manually afterwards, if you want it to appear immediately
    lk::Logger::stream().flush();

    // there are some predefined methods for logging prettier, but they include ANSI escape sequences etc.
    // and output a "file:line" (in debug builds, as decided by NDEBUG) as well as the severity.

    lk::log::debug() << "I'm a debug message" << std::endl;
    lk::log::info() << "I'm a normal info message" << std::endl;
    lk::log::warning() << "I'm a warning!" << std::endl;
    lk::log::error() << "I'm an error :(" << std::endl;
}
