#include <lk/Logger.h>
#include <lk/SQLite.h>

int main() {
    lk::Logger::the().add_stream(std::cout);
    const auto db_name = "my_database.db";
    lk::sqlite::Connection db(db_name);
    lk::sqlite::Statement create_stmt(db, R"(CREATE TABLE IF NOT EXISTS person(
        name TEXT,
        age INT
    );
    )");
    create_stmt.step_until_done();
    create_stmt.reset();
    lk::sqlite::Statement insert_stmt(db, "INSERT INTO person VALUES(:name, :age);");
    insert_stmt.bind(":name", "John Doe");
    insert_stmt.bind(":age", 45);
    insert_stmt.step_until_done();
    lk::log::info() << "created and wrote into database " << std::quoted(db_name) << std::endl;
}
