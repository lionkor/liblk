#include <iostream>
#include <lk/ArgsParser.h>
#include <lk/Logger.h> // for logging inside argsparser

int main(int argc, char** argv) {
    // logger must be initialized to get log messages
    lk::Logger::the().add_stream(std::cout);

    // make parser, give it arguments, parse
    lk::ArgsParser parser;
    parser.register_argument("help", lk::ArgsParser::NONE);
    parser.register_argument("say", lk::ArgsParser::HAS_VALUE);
    parser.parse(argc, argv);
    if (!parser.is_ok()) {
        return 1;
    }
    // handle arguments
    if (parser.found_argument({ "help" })) {
        std::cout << "Usage:\n"
                     "  --help            displays this help\n"
                     "  --say=<message>   prints the message"
                  << std::endl;
    } else if (parser.found_argument({ "say" })) {
        std::string value = parser.get_value_of_argument({ "say" }).value_or("EMPTY(?)");
        std::cout << value << std::endl;
    } else {
        lk::log::error() << "Expected arguments. Run '" << argv[0] << " --help' for a list of arguments." << std::endl;
    }
}
