#include <chrono>
#include <iostream>
#include <lk/Logger.h>
#include <lk/ThreadPool.h>

int main() {
    lk::Logger::the().add_stream(std::cout);

    lk::log::info() << "startup" << std::endl;

    const auto start = std::chrono::high_resolution_clock::now();

    std::atomic_size_t counter = 0;
    {
        lk::ThreadPool pool(8);

        for (size_t i = 0; i < 1'000'000; ++i) {
            (void)pool.add_task([&counter] {
                counter += 1;
            });
        }
    }

    const auto end = std::chrono::high_resolution_clock::now();

    lk::log::info() << "ran " << counter << " times, took " << (end - start).count() / 1000000 << "ms" << std::endl;
}
