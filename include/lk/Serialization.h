// Copyright (C) 2022 Lion Kortlepel
#pragma once

#include <array>
#include <bit>
#include <cassert>
#include <cmath>
#include <concepts>
#include <cstdint>
#include <cstring>
#include <iterator>
#include <lk/BufferView.h>
#include <vector>

namespace lk {

namespace detail {
    // modified version from https://stackoverflow.com/a/4887689, under CC BY-SA 2.5
    template<size_t N>
    void byteswap_array(uint8_t (&bytes)[N]) {
        for (uint8_t *p = bytes, *end = bytes + N - 1; p < end; ++p, --end) {
            uint8_t tmp = *p;
            *p = *end;
            *end = tmp;
        }
    }
}

// modified version from https://stackoverflow.com/a/4887689, under CC BY-SA 2.5
template<std::integral T>
T byteswap(T value) {
    detail::byteswap_array(*reinterpret_cast<uint8_t(*)[sizeof(value)]>(&value));
    return value;
}

class SerializationSizeCalculator {
public:
    constexpr SerializationSizeCalculator(size_t size = 0)
        : m_total_bytes(size) { }

    template<std::integral T>
    [[nodiscard]] constexpr SerializationSizeCalculator add() {
        return { m_total_bytes + sizeof(T) };
    }

    template<std::floating_point T>
    [[nodiscard]] constexpr SerializationSizeCalculator add() {
        // exp, x via frexp()
        return { m_total_bytes + sizeof(int) + sizeof(T) };
    }

    template<std::integral T>
    [[nodiscard]] constexpr SerializationSizeCalculator add(T) {
        return { m_total_bytes + sizeof(T) };
    }

    template<std::floating_point T>
    [[nodiscard]] constexpr SerializationSizeCalculator add(T) {
        return { m_total_bytes + sizeof(int) + sizeof(T) };
    }

    template<std::input_iterator IterT>
    [[nodiscard]] constexpr SerializationSizeCalculator add(const IterT& begin, const IterT& end) {
        return { m_total_bytes + sizeof(*begin) * (end - begin) };
    }

    constexpr SerializationSizeCalculator add_size(size_t size) {
        return { m_total_bytes + size };
    }

    [[nodiscard]] constexpr size_t result() const { return m_total_bytes; }

private:
    const size_t m_total_bytes { 0 };
};

template<std::integral T>
inline void serialize(BufferView& view, T n) noexcept {
    assert(view.wp + sizeof(n) <= view.data + view.size);
    if constexpr (std::endian::native != std::endian::big) {
        n = byteswap(n);
    }
    std::memcpy(view.wp, reinterpret_cast<uint8_t(*)[sizeof(n)]>(&n), sizeof(n));
    view.wp += sizeof(n);
}

template<std::floating_point T>
inline void serialize(BufferView& view, T f) {
    int exp {};
    T r = std::frexp(f, &exp);
    assert(view.wp + sizeof(exp) + sizeof(r) <= view.data + view.size);
    serialize(view, exp);
    serialize(view, *reinterpret_cast<uint64_t*>(&r));
}

template<std::input_iterator IterT>
inline void serialize(BufferView& view, const IterT& begin, const IterT& end) {
    for (IterT iter = begin; iter != end; ++iter) {
        serialize(view, *iter);
    }
}

template<typename T, std::input_iterator IterT>
inline void serialize(BufferView& view, const IterT& begin, const IterT& end) {
    for (IterT iter = begin; iter != end; ++iter) {
        serialize<T>(view, *iter);
    }
}

// TODO: Should be std::output_iterator, but requires a type
template<std::input_or_output_iterator IterT>
inline void deserialize(BufferView& view, IterT begin, IterT end) noexcept {
    for (IterT iter = begin; iter != end; ++iter) {
        deserialize(view, *iter);
    }
}

template<typename T, std::input_or_output_iterator IterT>
inline void deserialize(BufferView& view, IterT begin, IterT end) noexcept {
    for (IterT iter = begin; iter != end; ++iter) {
        deserialize<T>(view, *iter);
    }
}

template<std::integral T>
inline void deserialize(BufferView& view, T& n) noexcept {
    std::memcpy(reinterpret_cast<uint8_t(*)[sizeof(n)]>(&n), view.wp, sizeof(n));
    if constexpr (std::endian::native != std::endian::big) {
        n = byteswap(n);
    }
    view.wp += sizeof(n);
}

template<std::floating_point T>
inline void deserialize(BufferView& view, T& f) {
    int exp {};
    T r {};
    deserialize(view, exp);
    deserialize(view, *reinterpret_cast<uint64_t*>(&r));
    f = std::ldexp(r, exp);
}

}
