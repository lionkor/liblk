// Copyright (C) 2021, 2022 Lion Kortlepel
#pragma once

#include <atomic>
#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <mutex>
#include <ostream>
#include <queue>
#include <sstream>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <version>
#ifdef __cpp_lib_source_location
#include <source_location>
#endif

namespace lk {

using LogStream = std::ostream;

class Logger final : public std::stringbuf {
public:
    static inline Logger& the() {
        if (!s_instance) {
            auto ptr = new Logger;
            if (!ptr) {
                throw std::runtime_error("out of memory");
            }
            s_instance = std::unique_ptr<Logger>(ptr);
        }
        return *s_instance;
    }

    static inline std::ostream& stream() {
        if (!s_stream) {
            s_stream = std::make_unique<std::ostream>(&the());
        }
        return *s_stream;
    }

    ~Logger() {
        this->sync(); // calls this class' sync()
        m_cond.notify_one();
        m_shutdown = true;
        m_cond.notify_one();
        if (m_thread.joinable()) {
            m_thread.join();
        }
    }

    void add_stream(std::ostream& os) {
        std::unique_lock lock(m_streams_mutex);
        m_streams.emplace_back(os);
    }
    void add_file_stream(const std::string& path) {
        std::unique_lock lock(m_streams_mutex);
        auto& ref = m_file_streams.emplace_back(path);
        m_streams.emplace_back(ref);
    }

    void clear_streams() {
        std::unique_lock lock(m_streams_mutex);
        m_streams.clear();
        m_file_streams.clear();
    }

private:
    static inline std::unique_ptr<Logger> s_instance { nullptr };
    static inline std::unique_ptr<std::ostream> s_stream { nullptr };

    Logger()
        : m_thread(&Logger::thread_main, this) {
    }

    // all streams that will be written to
    std::vector<std::reference_wrapper<std::ostream>> m_streams;
    // just raii lifetime management for the filestreams owned by the logger
    std::vector<std::ofstream> m_file_streams;
    // strings which are queued to be written
    std::queue<std::string> m_queue;
    // mutex protecting the streams
    std::mutex m_streams_mutex;
    // mutex protecting the queue
    std::mutex m_queue_mutex;
    // condition variable to wake up the writing thread
    std::condition_variable m_cond;
    // writing thread
    std::thread m_thread;
    // signal to shut down the thread
    std::atomic_bool m_shutdown { false };

    // runs in a thread, mutex protect all memory accesses
    void thread_main() {
        while (!m_shutdown.load()) {
            std::unique_lock queue_lock(m_queue_mutex);
            m_cond.wait(queue_lock, [&] { return !m_queue.empty() || m_shutdown; });
            if (!m_shutdown && !m_queue.empty()) {
                auto msg = m_queue.front();
                m_queue.pop();
                queue_lock.unlock();
                std::unique_lock streams_lock(m_streams_mutex);
                for (auto& stream : m_streams) {
                    stream.get().write(msg.data(), msg.size());
                    stream.get().flush();
                }
            }
        }
        std::unique_lock streams_lock(m_streams_mutex);
        while (!m_queue.empty()) {
            const auto& msg = m_queue.front();
            for (auto& stream : m_streams) {
                stream.get().write(msg.data(), msg.size());
            }
            m_queue.pop();
        }
        for (auto& stream : m_streams) {
            stream.get() << std::flush;
        }
    }

    // basic_streambuf interface
protected:
    int sync() final override {
        try {
            std::unique_lock lock(m_queue_mutex);
            m_queue.push(this->str());
            this->str("");
        } catch (const std::exception& e) {
            std::cerr << "Logger encountered an exception: " << e.what() << std::endl;
            return -1;
        }
        m_cond.notify_one();
        return 0;
    }
};

// the following functions are implementations that use Logger, you may also just make your own :)
namespace log {

#ifdef __cpp_lib_source_location
    LogStream& error(const std::source_location& loc = std::source_location::current());
    LogStream& warning(const std::source_location& loc = std::source_location::current());
    LogStream& debug(const std::source_location& loc = std::source_location::current());
#else
    LogStream& error();
    LogStream& warning();
    LogStream& debug();
#endif
    LogStream& info();

}
}

template<typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& pair) {
    return os << "{ " << pair.first << ", " << pair.second << " }";
}
