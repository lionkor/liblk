#pragma once

#include <atomic>

namespace lk {

template<typename FnT>
static inline void do_once(FnT fn) {
    static std::atomic_bool s_done { false };
    bool off = false;
    if (s_done.compare_exchange_strong(off, true)) {
        fn();
    }
}

template<typename FnT>
static inline void do_once_per_thread(FnT fn) {
    static thread_local std::atomic_bool s_done { false };
    bool off = false;
    if (s_done.compare_exchange_strong(off, true)) {
        fn();
    }
}

}
