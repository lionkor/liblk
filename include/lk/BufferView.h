// Copyright (C) 2022 Lion Kortlepel
#pragma once

#include <cstddef>
#include <cstdint>

namespace lk {

struct BufferView {
    BufferView(uint8_t* ptr, size_t size)
        : size(size)
        , data(ptr)
        , wp(data) { }
    size_t size { 0 };
    uint8_t* data { nullptr };
    uint8_t* wp { nullptr };
    bool is_full() const {
        return wp >= data + size;
    }
};

}
