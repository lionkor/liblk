// Copyright (C) 2021, 2022 Lion Kortlepel
#pragma once

#include <initializer_list>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

namespace lk {
/*
 * Allows syntax:
 *  --help              : long flags
 *  --path=/home/lion   : long assignments
 */
class ArgsParser {
public:
    enum Flags : int {
        NONE = 0,
        REQUIRED = 1, // argument is required
        HAS_VALUE = 2, // argument must have a value
    };

    ArgsParser() = default;

    void parse(int argc, char** argv);
    void parse(const std::vector<std::string_view>& arg_list);

    // prints errors if any errors occurred, in that case also returns false
    bool is_ok();

    void register_argument(std::vector<std::string>&& argument_names, int flags);
    void register_argument(const std::string& argument_name, int flags);
    // pass all possible names for this argument (short, long, etc)
    bool found_argument(const std::vector<std::string>& names);
    std::optional<std::string> get_value_of_argument(const std::vector<std::string>& names);

private:
    void consume_long_argument(const std::string& arg);
    void consume_long_flag(const std::string& arg);
    bool is_registered(const std::string& name);

    struct Argument {
        std::string name;
        std::optional<std::string> value;
    };

    struct RegisteredArgument {
        std::vector<std::string> names;
        int flags;
    };

    std::vector<RegisteredArgument> m_registered_arguments;
    std::vector<Argument> m_found_args;
};
}
