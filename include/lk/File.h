#pragma once

#include <filesystem>
#include <fstream>
#include <mutex>
#include <span>
#include <vector>

namespace lk {

// super simple read-write file
class File {
public:
    File(const std::filesystem::path& path, const char* mode = "r+b");
    File(const File&) = delete;
    File& operator=(const File&) = delete;
    File(File&&);
    File& operator=(File&&);
    ~File();

    std::vector<uint8_t> read_all() const;
    void write_all(std::span<uint8_t>);

    void sync_to_disk();
    void hard_sync_to_disk();

    const std::filesystem::path& path() const;

    std::FILE* handle() const;

private:
    mutable std::mutex m_mutex;
    std::FILE* m_handle;
    std::filesystem::path m_path;
};

}
