#pragma once

#include <cstdio>
#include <filesystem>

namespace lk {

void zlib_compress(const std::filesystem::path& source_path, const std::filesystem::path& dest_path, int z_compression_level = 5);
void zlib_decompress(const std::filesystem::path& source_path, const std::filesystem::path& dest_path);

void zlib_compress_replace(const std::filesystem::path& path, int z_compression_level = 5);
void zlib_decompress_replace(const std::filesystem::path& path);

}
