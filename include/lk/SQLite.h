#pragma once

#include <cassert>
#include <cstdio>
#include <iostream>
#include <numeric>
#include <sqlite3.h>
#include <sstream>
#include <stdexcept>
#include <string>

namespace lk::sqlite {

struct Connection {
    sqlite3* ptr { nullptr };
    Connection(const char* filename);
    ~Connection();
    operator sqlite3*();
};

/*
 * 1. construct
 * 2. bind
 * 3. step
 */
struct Statement {
    sqlite3_stmt* ptr { nullptr };
    Connection& db;
    Statement(Connection& db, const char* raw_statement);
    ~Statement();
    operator sqlite3_stmt*();
    void reset();
    int index_of(const std::string& name);
    void bind(const std::string& name, const std::string& text_value);
    void bind(const std::string& name, int int_value);

    enum class What : bool {
        Row,
        Done,
    };

    [[nodiscard]] What step();
    void step_until_done();
};

}
