#pragma once

#include <condition_variable>
#include <cstdint>
#include <functional>
#include <mutex>
#include <queue>
#include <span>
#include <thread>

namespace lk {

class ThreadPool {
public:
    ThreadPool(size_t threads);
    ~ThreadPool();

    [[nodiscard]] std::shared_ptr<std::atomic<bool>> add_task(const std::function<void()>&);

    const std::chrono::high_resolution_clock::duration& timeout() const;
    void set_timeout(const std::chrono::high_resolution_clock::duration& new_timeout);

    static void wait_for_tasks(std::span<std::shared_ptr<std::atomic<bool>>>);
    size_t queued_job_count() const;
    bool queue_overfilled() const;

    bool is_busy(size_t thread_i) const;
    size_t thread_count() const;
    const std::vector<bool>& busy_list() const;

private:
    void task_handler_thread_main(size_t i);

    struct Task {
        std::shared_ptr<std::atomic<bool>> complete;
        std::function<void()> fn;
    };

    std::vector<std::thread> m_threads;

    bool m_shutdown { false };

    void wait() {
        if (m_shutdown) {
            return;
        }
        static thread_local size_t old_value = m_notifier;
        m_notifier.wait(old_value);
        old_value = m_notifier.load();
    }

    void notify_one() {
        m_notifier += 1;
        m_notifier.notify_one();
    }

    void notify_all() {
        m_notifier += 1;
        m_notifier.notify_all();
    }

    size_t m_thread_count;
    std::mutex m_tasks_mutex;
    std::vector<bool> m_busy;
    std::chrono::high_resolution_clock::duration m_timeout = std::chrono::milliseconds(500);
    // std::condition_variable m_tasks_cond;
    std::atomic_size_t m_notifier;
    std::queue<Task> m_tasks;
};

}
