#pragma once

#include <concepts>
#include <lk/Logger.h>
#include <stdexcept>
#include <string>

namespace lk {

template<std::derived_from<std::exception> ExceptionT>
class Error {
public:
    Error(const std::string& what, const std::string& error)
        : m_message(what + ": " + error + "")
        , m_exception(m_message) { log(); }

    const std::string& message() const { return m_message; }
    const ExceptionT& exception() const { return m_exception; }

private:
    void log() const {
        lk::log::error() << "Error: " << m_message << std::endl;
    }

    std::string m_message;
    ExceptionT m_exception;
};

using RuntimeError = Error<std::runtime_error>;
using LogicError = Error<std::logic_error>;

}
