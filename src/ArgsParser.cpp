#include <algorithm>
#include <lk/ArgsParser.h>
#include <lk/Logger.h>

using namespace lk;

void ArgsParser::parse(int argc, char** argv) {
    std::vector<std::string_view> args;
    args.reserve(argc - 1);
    // start at 1 since argv[0] is program name
    for (int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
    parse(args);
}

void ArgsParser::parse(const std::vector<std::string_view>& arg_list) {
    for (const auto& arg : arg_list) {
        if (arg.size() > 2 && arg.substr(0, 2) == "--") {
            // long arg
            if (arg.find("=") != arg.npos) {
                consume_long_argument(std::string(arg));
            } else {
                consume_long_flag(std::string(arg));
            }
        } else {
            log::error() << "Error parsing commandline arguments: Supplied argument '" + std::string(arg) + "' is not a valid argument and was ignored." << std::endl;
        }
    }
}

bool ArgsParser::is_ok() {
    bool ok = true;
    for (const auto& registered_arg : m_registered_arguments) {
        if (registered_arg.flags & Flags::REQUIRED && !found_argument(registered_arg.names)) {
            log::error() << "Error in commandline arguments: Argument '" + std::string(registered_arg.names.at(0)) + "' is required but wasn't found." << std::endl;
            ok = false;
            continue;
        } else if (found_argument(registered_arg.names)) {
            if (registered_arg.flags & Flags::HAS_VALUE) {
                if (!get_value_of_argument(registered_arg.names).has_value()) {
                    log::error() << "Error in commandline arguments: Argument '" + std::string(registered_arg.names.at(0)) + "' expects a value, but no value was given." << std::endl;
                    ok = false;
                }
            } else if (get_value_of_argument(registered_arg.names).has_value()) {
                log::error() << "Error in commandline arguments: Argument '" + std::string(registered_arg.names.at(0)) + "' does not expect a value, but one was given." << std::endl;
                ok = false;
            }
        }
    }
    return ok;
}

void ArgsParser::register_argument(std::vector<std::string>&& argument_names, int flags) {
    m_registered_arguments.push_back({ argument_names, flags });
}

void ArgsParser::register_argument(const std::string& argument_name, int flags) {
    m_registered_arguments.push_back({ { argument_name }, flags });
}

bool ArgsParser::found_argument(const std::vector<std::string>& names) {
    // if any of the found args match any of the names
    return std::any_of(m_found_args.begin(), m_found_args.end(),
        [&names](const Argument& arg) -> bool {
            // if any of the names match this arg's name
            return std::any_of(names.begin(), names.end(), [&arg](const std::string& name) -> bool {
                return arg.name == name;
            });
        });
}

std::optional<std::string> ArgsParser::get_value_of_argument(const std::vector<std::string>& names) {
    // finds an entry which has a name that is any of the names in 'Names'
    auto found = std::find_if(m_found_args.begin(), m_found_args.end(), [&names](const Argument& arg) -> bool {
        return std::any_of(names.begin(), names.end(), [&arg](const std::string_view& name) -> bool {
            return arg.name == name;
        });
    });
    if (found != m_found_args.end()) {
        // found
        return found->value;
    } else {
        return std::nullopt;
    }
}

bool ArgsParser::is_registered(const std::string& name) {
    return std::any_of(m_registered_arguments.begin(), m_registered_arguments.end(), [&name](const RegisteredArgument& arg) {
        auto iter = std::find(arg.names.begin(), arg.names.end(), name);
        return iter != arg.names.end();
    });
}

void ArgsParser::consume_long_argument(const std::string& arg) {
    auto value = arg.substr(arg.rfind("=") + 1);
    auto name = arg.substr(2, arg.rfind("=") - 2);
    if (!is_registered(name)) {
        log::warning() << "Argument '" + name + "' was supplied but isn't a known argument, so it is likely being ignored." << std::endl;
    }
    m_found_args.push_back({ name, value });
}

void ArgsParser::consume_long_flag(const std::string& arg) {
    auto name = arg.substr(2, arg.rfind("=") - 2);
    m_found_args.push_back({ name, std::nullopt });
    if (!is_registered(name)) {
        log::warning() << "Argument '" + name + "' was supplied but isn't a known argument, so it is likely being ignored." << std::endl;
    }
}
