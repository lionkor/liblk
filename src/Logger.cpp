// Copyright (C) 2022 Lion Kortlepel
#include <lk/Logger.h>

#ifdef __cpp_lib_source_location
static std::string debug_location_string(const std::source_location& loc) {
#ifndef NDEBUG
    return "[" + std::filesystem::path(loc.file_name()).filename().string() + ":" + std::to_string(loc.line()) + "] ";
#else
    return "";
#endif
}
#endif

namespace lk {

#ifdef __cpp_lib_source_location
LogStream& log::error(const std::source_location& loc) {
    return Logger::stream() << "[\x1b[0;31merror\x1b[0m] "
                            << debug_location_string(loc);
}

LogStream& log::warning(const std::source_location& loc) {
    return Logger::stream() << "[\x1b[0;33mwarning\x1b[0m] "
                            << debug_location_string(loc);
}

LogStream& log::debug(const std::source_location& loc) {
    return Logger::stream() << "[\x1b[2mdebug\x1b[0m] "
                            << debug_location_string(loc);
}
#else
LogStream& log::error() {
    return Logger::stream() << "[\x1b[0;31merror\x1b[0m] ";
}

LogStream& log::warning() {
    return Logger::stream() << "[\x1b[0;33mwarning\x1b[0m] ";
}

LogStream& log::debug() {
    return Logger::stream() << "[\x1b[2mdebug\x1b[0m] ";
}
#endif

LogStream& log::info() {
    return Logger::stream() << "[\x1b[0;36minfo\x1b[0m] ";
}

}
