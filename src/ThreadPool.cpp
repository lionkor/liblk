#include <lk/ThreadPool.h>

#include <lk/Logger.h>

lk::ThreadPool::ThreadPool(size_t threads) {
    m_busy.resize(threads, false);
    m_thread_count = threads;
    m_threads.reserve(threads);
    for (size_t i = 0; i < threads; ++i) {
        m_threads.emplace_back(&ThreadPool::task_handler_thread_main, this, i);
    }
    lk::log::debug() << "thread pool: started " << threads << " thread pool threads" << std::endl;
}

lk::ThreadPool::~ThreadPool() {
    m_shutdown = true;
    notify_all();
    for (auto& thread : m_threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }
}

std::shared_ptr<std::atomic<bool>> lk::ThreadPool::add_task(const std::function<void()>& fn) {
    Task task { std::make_shared<std::atomic<bool>>(false), fn };
    auto done = task.complete;
    {
        std::unique_lock lock(m_tasks_mutex);
        m_tasks.emplace(std::move(task));
    }
    notify_one();
    return done;
}

void lk::ThreadPool::task_handler_thread_main(size_t i) {
    while (true) {
        wait();
        std::unique_lock lock(m_tasks_mutex);
        if (!m_tasks.empty()) {
            auto task = std::move(m_tasks.front());
            m_tasks.pop();
            lock.unlock();
            m_busy[i] = true;
            task.fn();
            *task.complete = true;
            m_busy[i] = false;
        } else if (m_shutdown) {
            break;
        }
    }
}

const std::chrono::high_resolution_clock::duration& lk::ThreadPool::timeout() const {
    return m_timeout;
}

void lk::ThreadPool::set_timeout(const std::chrono::high_resolution_clock::duration& new_timeout) {
    m_timeout = new_timeout;
}

void lk::ThreadPool::wait_for_tasks(std::span<std::shared_ptr<std::atomic<bool>>> jobs) {
    while (true) {
        if (!std::all_of(jobs.begin(), jobs.end(), [](const auto& job) { return job->load(); })) {
            std::this_thread::yield();
            continue;
        } else {
            break;
        }
    }
}

size_t lk::ThreadPool::queued_job_count() const {
    return m_tasks.size();
}

bool lk::ThreadPool::queue_overfilled() const {
    return m_tasks.size() / 2 >= m_threads.size();
}

bool lk::ThreadPool::is_busy(size_t thread_i) const {
    return m_busy.at(thread_i);
}

namespace lk {
size_t ThreadPool::thread_count() const {
    return m_thread_count;
}

const std::vector<bool>& ThreadPool::busy_list() const {
    return m_busy;
}

}
