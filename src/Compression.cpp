#include <lk/Compression.h>

#include <array>
#include <cstdint>
#include <cstring>
#include <zlib.h>

#include <lk/Error.h>
#include <lk/File.h>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#include <fcntl.h>
#include <io.h>
#define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#define SET_BINARY_MODE(file)
#endif

#define LK_COMPRESSION_ZLIB_CHUNK 16384

static std::string get_zlib_error_string(int ret) {
    switch (ret) {
    case Z_ERRNO:
        return "Z_ERRNO";
    case Z_STREAM_ERROR:
        return "Z_STREAM_ERROR";
    case Z_DATA_ERROR:
        return "Z_DATA_ERROR";
    case Z_MEM_ERROR:
        return "Z_MEM_ERROR";
    case Z_VERSION_ERROR:
        return "Z_VERSION_ERROR";
    default:
        return "Unknown ZLIB error";
    }
}

void lk::zlib_compress(const std::filesystem::path& source_path, const std::filesystem::path& dest_path, int z_compression_level) {
    int ret {}, flush {};
    uint32_t have {};
    z_stream strm {};
    std::array<uint8_t, LK_COMPRESSION_ZLIB_CHUNK> in {};
    std::array<uint8_t, LK_COMPRESSION_ZLIB_CHUNK> out {};

    auto source_file = lk::File(source_path, "rb");
    auto dest_file = lk::File(dest_path, "wb");
    auto source = source_file.handle();
    auto dest = dest_file.handle();

    // allocate deflate state
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, z_compression_level);
    const auto generic_error_string = "zlib compression of " + source_path.string() + " to " + dest_path.string() + " failed";
    if (ret != Z_OK) {
        throw RuntimeError(generic_error_string + " on deflateInit()", get_zlib_error_string(ret)).exception();
    }

    do {
        strm.avail_in = std::fread(in.data(), 1, in.size(), source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            throw RuntimeError(generic_error_string, std::strerror(errno)).exception();
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in.data();

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = LK_COMPRESSION_ZLIB_CHUNK;
            strm.next_out = out.data();
            ret = deflate(&strm, flush); /* no bad return value */
            if (ret == Z_STREAM_ERROR) {
                throw RuntimeError(generic_error_string, "deflate() failed: " + get_zlib_error_string(ret)).exception();
            }
            have = LK_COMPRESSION_ZLIB_CHUNK - strm.avail_out;
            if (std::fwrite(out.data(), 1, have, dest) != have || ferror(dest)) {
                (void)deflateEnd(&strm);
                throw RuntimeError(generic_error_string, std::strerror(errno)).exception();
            }
        } while (strm.avail_out == 0);
        if (strm.avail_in != 0) {
            throw RuntimeError(generic_error_string, "strm.avail_in != 0").exception();
        }
        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    (void)deflateEnd(&strm);
    if (ret != Z_STREAM_END) {
        throw RuntimeError(generic_error_string, "expected stream end, but didn't get it").exception();
    }
}

void lk::zlib_decompress(const std::filesystem::path& source_path, const std::filesystem::path& dest_path) {
    int ret {};
    uint32_t have {};
    z_stream strm {};
    std::array<uint8_t, LK_COMPRESSION_ZLIB_CHUNK> in {};
    std::array<uint8_t, LK_COMPRESSION_ZLIB_CHUNK> out {};

    auto source_file = lk::File(source_path, "rb");
    auto dest_file = lk::File(dest_path, "wb");
    auto source = source_file.handle();
    auto dest = dest_file.handle();

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    const auto generic_error_string = "zlib decompression of " + source_path.string() + " to " + dest_path.string() + " failed";
    if (ret != Z_OK) {
        throw RuntimeError(generic_error_string + " on inflateInit()", get_zlib_error_string(ret)).exception();
    }

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = std::fread(in.data(), 1, in.size(), source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            throw RuntimeError(generic_error_string, std::strerror(errno)).exception();
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in.data();

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = LK_COMPRESSION_ZLIB_CHUNK;
            strm.next_out = out.data();
            ret = inflate(&strm, Z_NO_FLUSH);
            if (ret == Z_STREAM_ERROR) {
                throw RuntimeError(generic_error_string, "inflate() failed: " + get_zlib_error_string(ret)).exception();
            }
            have = LK_COMPRESSION_ZLIB_CHUNK - strm.avail_out;
            if (std::fwrite(out.data(), 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
                throw RuntimeError(generic_error_string, std::strerror(errno)).exception();
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    if (ret != Z_STREAM_END) {
        throw RuntimeError(generic_error_string, "expected stream end, but didn't get it").exception();
    }
}

void lk::zlib_compress_replace(const std::filesystem::path& path, int z_compression_level) {
    auto newpath = path.string() + ".lk.temp";
    zlib_compress(path, newpath, z_compression_level);
    auto copypath = path.string() + ".lk.temp.backup";
    std::filesystem::rename(path, copypath);
    std::filesystem::rename(newpath, path);
    std::filesystem::remove(copypath);
}

void lk::zlib_decompress_replace(const std::filesystem::path& path) {
    auto newpath = path.string() + ".lk.temp";
    zlib_decompress(path, newpath);
    auto copypath = path.string() + ".lk.temp.backup";
    std::filesystem::rename(path, copypath);
    std::filesystem::rename(newpath, path);
    std::filesystem::remove(copypath);
}
