#ifdef __linux
#define _POSIX_SOURCE
#include <unistd.h>
#endif

#include <cstdio>
#include <cstring>
#include <errno.h>
#include <exception>
#include <lk/Error.h>
#include <lk/File.h>
#include <lk/Logger.h>
#include <stdexcept>

lk::File::File(const std::filesystem::path& path, const char* mode)
    : m_handle(std::fopen(path.c_str(), mode))
    , m_path(path) {
    if (!m_handle) {
        throw RuntimeError(std::strerror(errno), path.string()).exception();
    }
    std::fseek(m_handle, 0, SEEK_SET);
}

lk::File& lk::File::operator=(File&& other) {
    m_path = std::move(other.m_path);
    m_handle = std::move(other.m_handle);
    other.m_handle = nullptr;
    other.m_path = "";
    return *this;
}

lk::File::~File() {
    if (m_handle) {
        sync_to_disk();
        std::fclose(m_handle);
        m_handle = nullptr;
    }
}

lk::File::File(lk::File&& other)
    : m_handle(std::move(other.m_handle))
    , m_path(std::move(other.m_path)) {
    other.m_handle = nullptr;
    other.m_path = "";
}

std::vector<uint8_t> lk::File::read_all() const {
    std::unique_lock lock(m_mutex);
    if (!m_handle) {
        throw RuntimeError("empty handle for file", m_path.string()).exception();
    }
    auto size = std::filesystem::file_size(m_path);
    std::vector<uint8_t> buffer(size);
    std::fseek(m_handle, 0, SEEK_SET);
    auto n = std::fread(buffer.data(), sizeof(uint8_t), buffer.size(), m_handle);
    if (n != size) {
        throw RuntimeError(std::strerror(errno), m_path.string()).exception();
    }
    return buffer;
}

void lk::File::write_all(std::span<uint8_t> data) {
    std::unique_lock lock(m_mutex);
    if (!m_handle) {
        throw lk::RuntimeError("empty handle for file", m_path.string()).exception();
    }
    if (data.empty()) {
        lk::log::warning() << "writing empty data to file " << m_path << std::endl;
    }
    std::fseek(m_handle, 0, SEEK_SET);
    auto n = std::fwrite(data.data(), sizeof(uint8_t), data.size(), m_handle);
    if (n != data.size_bytes()) {
        throw RuntimeError(std::strerror(errno), m_path.string()).exception();
    }
}

void lk::File::sync_to_disk() {
    std::unique_lock lock(m_mutex);
    std::fflush(m_handle);
}

void lk::File::hard_sync_to_disk() {
    sync_to_disk();
    std::unique_lock lock(m_mutex);
#if defined(_POSIX_SOURCE)
    int fd = fileno(m_handle);
    fsync(fd);
#else
    lk::log::warning() << "hard_sync_to_disk is not implemented fully on this platform" << std::endl;
#endif
}

const std::filesystem::path& lk::File::path() const {
    return m_path;
}

namespace lk {
std::FILE* File::handle() const {
    return m_handle;
}

}
