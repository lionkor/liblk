#include <lk/Error.h>
#include <lk/SQLite.h>

static inline void sqlite_check_and_throw(sqlite3* db, int rc) {
    if (rc != SQLITE_OK) {
        throw lk::RuntimeError("sqlite3", sqlite3_errmsg(db)).exception();
    }
}

lk::sqlite::Connection::Connection(const char* filename) {
    int rc = sqlite3_open(filename, &ptr);
    sqlite_check_and_throw(ptr, rc);
}

lk::sqlite::Connection::~Connection() {
    int rc = sqlite3_close(ptr);
    sqlite_check_and_throw(ptr, rc);
}

lk::sqlite::Connection::operator sqlite3*() {
    return ptr;
}

lk::sqlite::Statement::Statement(Connection& db, const char* raw_statement)
    : db(db) {
    int rc = sqlite3_prepare_v2(db, raw_statement, -1, &ptr, nullptr);
    sqlite_check_and_throw(db, rc);
}

lk::sqlite::Statement::~Statement() {
    int rc = sqlite3_finalize(ptr);
    sqlite_check_and_throw(db, rc);
}

lk::sqlite::Statement::operator sqlite3_stmt*() {
    return ptr;
}

void lk::sqlite::Statement::reset() {
    int rc = sqlite3_reset(ptr);
    sqlite_check_and_throw(db, rc);
}

int lk::sqlite::Statement::index_of(const std::string& name) {
    int name_index = sqlite3_bind_parameter_index(ptr, name.c_str());
    if (name_index == 0) {
        throw std::runtime_error("parameter '" + name + "' wasn't found");
    }
    return name_index;
}

void lk::sqlite::Statement::bind(const std::string& name, const std::string& text_value) {
    int index = index_of(name);
    int rc = sqlite3_bind_text(ptr, index, text_value.c_str(), text_value.size(), nullptr);
    sqlite_check_and_throw(db, rc);
}

void lk::sqlite::Statement::bind(const std::string& name, int int_value) {
    int index = index_of(name);
    int rc = sqlite3_bind_int(ptr, index, int_value);
    sqlite_check_and_throw(db, rc);
}

lk::sqlite::Statement::What lk::sqlite::Statement::step() {
    int rc = sqlite3_step(ptr);
    if (rc == SQLITE_ROW) {
        return What::Row;
    } else if (rc == SQLITE_DONE) {
        return What::Done;
    } else {
        throw std::runtime_error(sqlite3_errmsg(db));
    }
}

void lk::sqlite::Statement::step_until_done() {
    while (step() != What::Done)
        ;
}
