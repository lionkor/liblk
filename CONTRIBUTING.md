# Contributing

To contribute to `liblk`, fork the repository, make changes, and open a pull-/merge-request.
Changes need to be formatted according to the .clang-format file supplied in the root of the repository, and haved to pass the provided cppchecks.

You can check for formatting and cppcheck compatibility by running `./tools/cppcheck.sh` and `./tools/clang-format.sh` respectively. If they exit with a non-zero exit-code, changes need to be adjusted.

Further, you should ensure that, if there were any changes to documented features, the documentation is adjusted accordingly.

## Pull-/Merge-Requests

By doing a merge-request, your code will keep its copyright but will be licensed under the project's [AGPL License](./LICENSE).
